# RE-TAG IMAGES OR MOVE BETWEEN REGISTRIES

[[_TOC_]]

## Description

Crane is a very flexible tool for retagging, renaming, moving, copying container images. This specific template will allow you:

- Retag an image
- Rename an image
- Copy an image from one registry to another
- All three of the above in combination if you wish

### Available Variables

| Name | Default Value | Notes |
| ---  | ---           | --- |
| SOURCE_REGISTRY | `"registry.gitlab.com"` | The registry of the source image |
| SOURCE_IMAGE_NAME | `$CI_PROJECT_PATH` | The path and name of your source image |
| SOURCE_TAG | `$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA` | The tag of the source image |
| SOURCE_USER | `$CI_REGISTRY_USER` | CICD Variable containing the username to log into the source registry |
| SOURCE_PASSWORD | `$CI_REGISTRY_PASSWORD` | CICD Variable containing password to log into the source registry |
| DEST_REGISTRY | `"registry.gitlab.com"` | The registry of the source image |
| DEST_IMAGE_NAME | `$CI_PROJECT_PATH` | The path and name of the image destination |
| DEST_TAG | `$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA` | The tag of the image destination |
| DEST_USER | `$CI_REGISTRY_USER` | CICD Variable containing the username to log into the destination registry |
| DEST_PASSWORD | `$CI_REGISTRY_PASSWORD` | CICD Variable containing password to log into the destination registry |

### Default Usage 

The default usage in this case would retag an image in a Gitlab registry that was created in your branch to :latest. Its not the most usual case but common usage variations are  described below.

```yaml
include:
    - remote: 'https://gitlab.com/ingka/templates/raw/master/container/retag-and-push/retag-image.yml'
```

### Common Usages

#### Tag an image with the release version

```yaml
include:
    - remote: 'https://gitlab.com/ingka/templates/raw/master/container/retag-and-push/retag-image.yml'
variables:
    DEST_TAG: $CI_COMMIT_TAG
```

#### Copy and image from GCR dev project to GCR production project (whilst retagging with release)

```yaml
include:
    - remote: 'https://gitlab.com/ingka/templates/raw/master/container/retag-and-push/retag-image.yml'
variables:
    SOURCE_REGISTRY: "eu.gcr.io"
    SOURCE_IMAGE_NAME: "dev-project/$CI_PROJECT_NAME"
    SOURCE_TAG: $CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA
    SOURCE_USER: "_json_key"
    SOURCE_PASSWORD: $CI_VARIABLE_WITH_DEV_SA_JSON
    DEST_REGISTRY: "eu.gcr.io"
    DEST_IMAGE_NAME: "prod-project/$CI_PROJECT_NAME"
    DEST_TAG: "$CI_COMMIT_TAG"
    DEST_USER: "_json_key"
    DEST_PASSWORD: $CI_VARIABLE_WITH_PROD_SA_JSON
```

#### Reusing this template multiple times in different stages of your pipeline

You perhaps will retag images multiple times and want to re-use this job under different names and at different times.

Here is an example where we retag as latest after a successful master branch pipeline, then use it again to tag the release version.

```yaml
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/container/retag-and-push/retag-image.yml'

retag_latest:
  extends: retag-image
  stage: package
  rules:
    - if: $CI_COMMIT_BRANCH
      when: never

retag_release:
  extends: retag-image
  stage: build
  variables:
    DEST_TAG: "$CI_COMMIT_TAG"
  rules:
    - if: $CI_COMMIT_BRANCH
      when: never
    - if: $CI_COMMIT_TAG
      when: on_success
      allow_failure: false
```