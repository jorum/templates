# CONTAINER TEMPLATES

[[_TOC_]]

## Description

These templates are used for working with container images and pushing / tagging them with various types of registry.

See https://docs.gitlab.com/ee/ci/yaml/includes.html for how to use templates or the README.md in each of the jobs in this repository for specific examples

## Tools Used

### Kaniko

Kaniko is a Google project (https://github.com/GoogleContainerTools/kaniko) used for building Docker images instead of Docker for a few reasons but mainly security. 

The typical method of building images is to use Docker in Docker (DinD) which is both resource intensive and insecure. Kaniko solves "most" of the problems (does not need a priveledged pod to run in, mapping of docker sockets, etc) whilst still being easy to use.

### Crane

Crane (https://github.com/google/go-containerregistry/tree/master/cmd/crane) is also a Google project for working with built images, especially tagging, retagging and moving them between registries. This is used in such jobs for the same reason as we use Kaniko, to avoid DinD jobs at all costs.

## Avoiding Docker Hub Rate Limits

Docker hub rate limits mean that busy CI environments can quickly hit their new restrictions for how many layers can be pulled from them in each day.

For that reason these jobs endeavour to use https://mirror.gcr.io wherever possible as a mirror. However, less common images are not likely to be in Google's mirror and thus DockerHub is the fallback option.

This makes the jobs seem a little more complex but they are essentially just trying once against the mirror and if it fails then trying against Dockerhub.

### Kaniko and Mirror Registries

Kaniko should treat mirror.gcr.io as a ´pull-through' mirror but currently it does not - I (and others) have raised a bug with the project already

### The Dependency Proxy Alternative

Gitlab has a built-in Dependency Proxy (for Container images as well as other types of repo such as NPM, Maven, etc).

Navigate to Packages & Registries > Dependency Proxy in your project to get the URL and modify your Dockerfile! More documentation can be found here https://docs.gitlab.com/ee/user/packages/dependency_proxy/

