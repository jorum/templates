# BUILD AND PUSH CONTAINER IMAGE TEMPLATES

[[_TOC_]]

# DESCRIPTION
Templates in this directory will build container images using Kaniko and push to them to relevant registries.
# TEMPLATES

## build-image-gl.yml

Using Gitlab's image registry is by far the easiest, cheapest and most useful way to store your images! 

You pay nothing for storage and Gitlab's registries still use GCR under the hood! You also need provide no credentials as the job will automatically utilise the credentials of the user who initiated the pipeline.

### Usage

All that is needed to build, tag and push your container image is one line! No configuration required!

```yaml
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/container/build/build-image-gl.yml'
```

This will build your image based on a Dockerfile at the root of your project with the name `registry.gitlab.com/your-group/your-project:branch-SHORT_COMMIT_SHA`
### Customisation

Whilst the default settings will likely satisfy 80% of cases, there may be times you wish to modify things. The relevant properties are all exposed as variables which you can modify.

#### Available Variables

| Name | Default Value |
|---   | ---   |
| DOCKERFILE: | `'Dockerfile'` |
| CONTEXT: | `$CI_PROJECT_DIR` |
| REGISTRY: | `$CI_REGISTRY` |
| REGISTRY_MIRROR: | `'mirror.gcr.io'` |
| IMAGE_NAME: | `$CI_PROJECT_PATH` |
| IMAGE_TAG: | `$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA` |
| USER_NAME: | `$CI_REGISTRY_USER` |
| PASSWORD: | `$CI_REGISTRY_PASSWORD` |

#### Overriding Variables

Simply override the variables in your `.gitlab-ci.yml`.

For example, to override the `IMAGE_NAME` and the `IMAGE_TAG` whilst using a `DOCKERFILE` under another path, you would have the following in your `.gitlab-ci.yml`:

```yaml
include:
    - remote: 'https://gitlab.com/ingka/templates/raw/master/container/build/build-image-gl.yml'
build-image-gl:
  variables:
    CONTEXT: 'path/to/my/dockerfile/'
    IMAGE_NAME: 'another-name'
    IMAGE_TAG: 'another-tag'
```
## build-to-gcr.yml

Occasionally it is necessary for your image to be pushed to Google Container Registry (for example Cloud Run deployments require it). This template will build and store your image in GCR if you provide a service account to use.

### Usage

First you must create a CI/CD Variable that contains your GCP Service Account (with the storage admin role) json key. If you use the name `GCR_REGISTRY_SA` then all you need to run this job is then to include the following lines: in your .gitlab-ci.yml

```yaml
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/container/build/build-image-gcr.yml'
build-image-gcr:
  variables:
    GCP_PROJECT: your_gcp_project_id
```

This will build your image based on a Dockerfile at the root of your project with the name `eu.gcr.io/gcp_project_id/gitlab_project:branch-SHORT_COMMIT_SHA`
### Customisation

Whilst the default settings will likely satisfy 80% of cases, there may be times you wish to modify things. The relevant properties are all exposed as variables which you can modify.

#### Available Variables

| Name | Default Value |
|---   | ---   |
| DOCKERFILE: | `'Dockerfile'` |
| CONTEXT: | `$CI_PROJECT_DIR` |
| REGISTRY: | `eu.gcr.io` |
| GCP_PROJECT: | `null` |
| REGISTRY_MIRROR: | `'mirror.gcr.io'` |
| IMAGE_NAME: | `$CI_PROJECT_NAME` |
| IMAGE_TAG: | `$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA` |
| SERVICE_ACCOUNT: | `$GCR_REGISTRY_SA` |

#### Overriding Variables

Simply override the variables in your `.gitlab-ci.yml`.

For example, to override the `IMAGE_NAME` and the `IMAGE_TAG` whilst using a `DOCKERFILE` under another path, you would have the following in your `.gitlab-ci.yml`:

```yaml
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/container/build/build-image-gcr.yml'
build-image-gcr:
  variables:
    GCP_PROJECT: your_gcp_project_id
    CONTEXT: 'path/to/my/dockerfile/'
    IMAGE_NAME: 'another-name'
    IMAGE_TAG: 'another-tag'
```

Or to provide different credentials stored under another CI/CD Variable name you would provide:
```yaml
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/container/build/build-image-gcr.yml'
build-image-gcr:
  variables:
    GCP_PROJECT: your_gcp_project_id
    SERVICE_ACCOUNT: '$ANOTHER_CICD_VARIABLE'
```
