# DEPLOY TO MANAGED CLOUD RUN

[[_TOC_]]

## Description

This template performs a deployment to Cloud Run with the image and settings of your choice. This is a job you may use several times in a pipeline and examples of how to use it multiple times are below.

The template will also connect the deployed environment to the UI of Gitlab including capturing the URL

### Available Variables

| Name | Default Value | Notes |
| ---  | ---           | --- |
| CR_SERVICE_NAME | `$CI_PROJECT_NAME` | The service name to give to your deployment |
| CR_PORT | `"8080"` | The port exposed in your container |
| CR_GCP_PROJECT | `"my-project"` | **MANDATORY:** The GCP Project ID in which to deploy |
| CR_DEPLOY_SA | `$DEV_CLOUDRUN_DEPLOY_SA` | **MANDATORY:** A CICD Variable holding the Service Account JSON key to use |
| CR_APPLICATION_SA | "" | Optional service account to be activated within the cloudrun container for access to Google Services |
| CR_GCP_REGION | `"europe-west1"` | Region to deploy in |
| CR_REVISION_SUFFIX | `$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA` | A prefix added to the revision of your deployment |
| CR_REGISTRY | `"eu.gcr.io"` | The registry of the image to deploy (this will always need to be a GCR registry) |
| CR_IMAGE_NAME | `$CI_PROJECT_NAME` | The name (and path if required) of the image to deploy |
| CR_IMAGE_TAG | `$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA` | The image tag of the image to deploy |
| CR_MEM_REQUEST | `"256Mi"` | Container's memory requirement |
| CR_CPU_REQUEST | `"1"` | Container's CPU requirement |
| CR_ENV_VARS | `"DEPLOYED_BY=gitlab"` | A comma seperated list of environment variables the container may require. e.g. `KEY1=VALUE1, KEY2=VALUE2`|
| CR_REQUEST_TIMEOUT | `"5m"` | Allowed timeout for requests to your deployment before they return a 504 |
| CR_CONCURRENCY | `"80"` | The number of concurrent requests allowed to a single instance of your container |
| CR_MIN_INSTANCES | `"0"` | The minimum number of instances of the container that should be running at all times |
| CR_MAX_INSTANCES | `"100"` | The maximum number of instances of the container allowed |
| CR_ALLOW_UNAUTHENTICATED | `"--no-allow-unauthenticated"` | Replace with `"--allow-unauthenticated"` if you wish to make the service public |
| CR_EXTRA_FLAGS | `""` | Allows you to pass additonal flags such as "--vpc-connector my-connector" as features get added to Cloud Run in future |

### Default Usage 

The default usage in would require a minimum of 2 variables (CR_GCP_PROJECT and CR_SERVICE_ACCOUNT). With those specifed you would get a deployment from your branch with all the default settings. Things like the name, revision and sizing of the project will be set based on your Project's name, branch, SHORT COMMIT SHA and Google's default settings.

```yaml
include:
    - remote: 'https://gitlab.com/ingka/templates/raw/master/deploy/cloudrun/deploy.yml'
deploy:
  variables:
    CR_GCP_PROJECT: "my-dev-gcp-project"
    CR_SERVICE_ACCOUNT: $DEV_CLOUDRUN_DEPLOY_SA
```

### Common Usages

#### Specify Different Image & Sizings

```yaml
include:
    - remote: 'https://gitlab.com/ingka/templates/raw/master/deploy/cloudrun/deploy.yml'
deploy:
  variables:
    CR_GCP_PROJECT: "my-dev-gcp-project"
    CR_SERVICE_ACCOUNT: $DEV_CLOUDRUN_DEPLOY_SA
    CR_REGISTRY: "gcr.io"
    CR_IMAGE_NAME: some-other-image
    CR_IMAGE_TAG: $CI_COMMIT_TAG
    CR_MEM_REQUEST: "512Mi"
    CR_CPU_REQUEST: "2"
```

#### Multiple deployments throughout the pipeline

```yaml
# Would be used only for deploying feature branches
include:
    - remote: 'https://gitlab.com/ingka/templates/raw/master/deploy/cloudrun/deploy.yml'
deploy:
  variables:
    CR_GCP_PROJECT: "my-dev-gcp-project"
    CR_SERVICE_ACCOUNT: $DEV_CLOUDRUN_DEPLOY_SA

# Re-use for a staging deploy 
deploy-staging:
  extends: deploy
  variables:
    CR_GCP_PROJECT: "my-dev-gcp-project"
    CR_SERVICE_ACCOUNT: $DEV_CLOUDRUN_DEPLOY_SA
    CR_IMAGE_TAG: $CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA

# Re-use for a production deploy
deploy-production:
  extends: deploy
  variables:
    CR_GCP_PROJECT: "my-prod-gcp-project"
    CR_SERVICE_ACCOUNT: $PROD_CLOUDRUN_DEPLOY_SA
    CR_IMAGE_TAG: $CI_COMMIT_TAG
```
