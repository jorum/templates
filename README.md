# GITLAB TEMPLATES

[[_TOC_]]

## What are Gitlab Templates

It is kind of boring to write CI or CD job from scratch every time. Sure you can copy / paste jobs from one project to another, but better is to utilise templates which can either be used directly or extended to fit specific cases and peculiarities of your specific case.

Gitlab has utilised templates almost since it's inception - and of course they have an entire Auto DevOps offering that is built entirely on their templating. But much more useful is to provide organisation-scoped templates to save time and wheel re-inventing whilst simplifying greatly effort in a project to maintain the pipeline.

Every Gitlab pipeline is based on a `.gitlab-ci.yml` at the root of your project. You can (if you wish) write every single job in that file in great detail, or you can simply use it as a "wrapper" to pull in templates and extend them.

### Without a Template
```yaml
stages:
  - build
build-image:
  stage: build
  tags:
    - dev
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: ["/busybox/sh"]
  script: |
        export GOOGLE_APPLICATION_CREDENTIALS="$GCP_DEV_DEPLOY_SA"
        source cicd/shared-scripts/image-name.sh
        echo "Building image: $IMAGE"
        /kaniko/executor \
        --use-new-run=true \
        --registry-mirror=mirror.gcr.io \
        --context="$CI_PROJECT_DIR" \
        --dockerfile="$CI_PROJECT_DIR/Dockerfile" \
        --destination="$IMAGE"
  artifacts:
    paths:
      - image.txt
  rules:
    - if: $CI_COMMIT_BRANCH
      when: always
      allow_failure: false
    - if: $CI_COMMIT_TAG
      when: on_success
      allow_failure: false
```

### With a Template

```yaml
include: 'https://gitlab.com/ingka/templates/raw/master/container/build/build-to-gcr.yml'
```

It is certainly cleaner and easier for a project to utilise a template that already does what they need without needing to re-invent that wheel. It also makes it easier to apply the same CI job change to multiple projects simulataneously.

## Available Templates

### In this repository

#### Browser Performance
- [Use Sitespeed to test UI performance in a variety of browsers](browser-performance/sitespeed)

#### Code Navigation

- [Add code navigation to various parts of the Gitlab UI](code-navigation/)
- [ESlint for Javascript and Node](code-quality/eslint)

#### Commit Lint

- [Lint against conventional commit config](commit/conventional)

#### Code Quality

- [Use CodeClimate to find problems in your code's quality](code-quality/codeclimate)

#### Containers

- [Build and push container image to Gitlab Registry or GCR using Kaniko](container/build/)
- [Scan your container image for vulnerabilities using Trivy](image-scan/trivy)
- [Retag, rename and/or copy images - even between different registries](container/retag-and-push)

#### Deploy

- [Deploy application to Cloud Run](deploy/cloudrun)
- [Deploy application to Kubernetes](deploy/) # Coming Soon
#### Dynamic Application Security Testing

- [Gitlab DAST utilising OWASP ZAProxy for Sites/APIs](dast/gitlab)
#### Load / Performance / Stress Testing
- [Using K6.io](load-testing/k6)

#### Release Jobs
- [Check Semantic Version format](release/check-semantic-version)
- [Automated Semantic Release](release/semantic-release)
- [Post Release Notes to Slack](release/publish-mr-notes-to-slack/) # Not a template for now - but an example

#### Software Composition Analysis

- [Blackduck scanning for all languages](software-composition/blackduck)
- [Node.js: audit-ci](software-composition/nodejs-audit-ci)
- [Node.js / Javascript RetireJS](software-composition) # Coming soon

#### Static Application Security Testing

- [Gitlab SAST utilising a wide range of tools](dast/gitlab)
- [FindSecBugs for Java Applications](sast/java/findsecbugs) # In Testing
- [Gosec for Golang Applications](sast/golang/gosec)
- [NodeJsScan for NodeJS applications](sast/nodejs/nodejsscan)
- [ES Lint Security Plugin for JavaScript and React](sast/) # Coming Soon

#### Testing

- [Golang Unit testing](unit-test/golang)
- [Java (Maven) Unit testing](unit-test/java-mvn) # In Testing
- [Node.js Unit testing](unit-test/nodejs)
- [Cypress front-end / E2E testing](testing/cypress)

### Gitlab's own templates

- [Auto Devops - An entire pipeline in a single line!](https://docs.gitlab.com/ee/topics/autodevops/)
- [Auto Devops Components - Just borrow part of the Auto Devops pipeline!](https://gitlab.com/gitlab-org/gitlab/-/tree/master/lib/gitlab/ci/templates/Jobs)

### Other Examples

- https://docs.gitlab.com/ee/ci/examples/

## How to use templates

### Include the template in your pipeline

Templates can be included from your own project, other projects, any HTTP location, or directly from Gitlab or another provider. Here are some examples of includes

```yaml
include:
  # Any public HTTP location
  - 'https://gitlab.com/awesome-project/raw/master/.before-script-template.yml'
  # A template within your project
  - '/templates/.after-script-template.yml'
  # One of Gitlab's pre-built templates
  - template: Auto-DevOps.gitlab-ci.yml
  # Any Gitlab project to which you have access
  - project: 'my-group/my-project'
    ref: master
    file: '/templates/.gitlab-ci-template.yml'
```

## Override values in a Template

Most templates will expose variables that can be overwritten by you. Some may even have a requirement to provide one or more of the variables:

### Example

```yaml
include:
    - remote: 'https://gitlab.com/ingka/templates/raw/master/container/build/build-to-gcr.yml'
```

This is a job that will build and push your docker image to GCR. It makes some default decisions for you which you may wish to override and one variable `GCP_PROJECT` in particular is required from you 

| Variable Name | Default Value |
| ---           | ---           |
| DOCKERFILE: | `'Dockerfile'` |
| CONTEXT: | `$CI_PROJECT_DIR` |
| REGISTRY: | `eu.gcr.io` |
| GCP_PROJECT: | `null` |
| REGISTRY_MIRROR: | `'mirror.gcr.io'` |
| IMAGE_NAME: | `$CI_PROJECT_NAME` |
| IMAGE_TAG: | `$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA` |
| SERVICE_ACCOUNT: | `$GCR_REGISTRY_SA` |

If you wish to accept all the defaults and just provide the `GCP_PROJECT` for this job, then your `.gitlab-ci.yml` would look like:

```yaml
include:
    - remote: 'https://gitlab.com/ingka/templates/raw/master/container/build/build-to-gcr.yml'
variables:
    GCP_PROJECT: your_gcp_project_id
```

But if you wish to override the `IMAGE_NAME` and the `IMAGE_TAG` whilst using a `DOCKERFILE` under another path, you would have the following in your `.gitlab-ci.yml`:

```yaml
include:
    - remote: 'https://gitlab.com/ingka/templates/raw/master/container/build/build-to-gcr.yml'
variables:
    GCP_PROJECT: your_gcp_project_id
    CONTEXT: 'path/to/my/dockerfile/'
    IMAGE_NAME: 'another-name'
    IMAGE_TAG: 'another-tag'
```

## Extending a Template

Perhaps you need a script to run, or to generate an .env file, or other thing that is not included in the template. No problem. 

### before_script

Using the same example as above I would now have in my `.gitlab-ci.yml`

```yaml
include:
    - remote: 'https://gitlab.com/ingka/templates/raw/master/container/build/build-to-gcr.yml'
variables:
    GCP_PROJECT: your_gcp_project_id
build-image-gcr:
    before_script: 'source cicd/generate_env_file.sh'
```

### Extends

Coming Soon

### YAML Anchors

Coming Soon

## Adding Runner Tags

Perhaps you want to make sure gitlab uses your own private runner for a particular job?

```yaml
include:
    - remote: 'https://gitlab.com/ingka/templates/raw/master/container/build/build-to-gcr.yml'
variables:
    GCP_PROJECT: your_gcp_project_id
build-image-gcr:
    tags:
        - my-project-runner-tag1
        - my-project-runner-tag1
    before_script: 'source cicd/generate_env_file.sh'
```

## Adding anything else from Gitlab CI Syntax

The pattern is the same as the Adding Runner Tags example above. Essentially repeat the jobs name and add anthing you like from the https://docs.gitlab.com/ee/ci/yaml/. Essentially, the gitlab runner then merges the template's definiton with your own custom definitions, overwriting defaults with your own values if they clash.

```yaml
include:
    - remote: 'https://gitlab.com/ingka/templates/raw/master/container/build/build-to-gcr.yml'
variables:
    GCP_PROJECT: your_gcp_project_id
# JOB SPECIFIC CI OVERRIDES START HERE
build-image-gcr:
  tags:
    - my-project-runner-tag1
    - my-project-runner-tag1
  before_script: 'source cicd/generate_env_file.sh'
  stage: a-different-stage
  artifacts:
    paths:
      - image-name.txt
  rules:
    - if: $CI_COMMIT_BRANCH == "very-special-branch"
      when: always
      allow_failure: true

the-next-job:
  tags:
    - kubernetes
  image: alpine:latest

... etc
```
