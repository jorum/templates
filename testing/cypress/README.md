# CYPRESS E2E / INTEGRATION TESTING

[[_TOC_]]

## Description

Cypress is a front end testing suite that allows you to run tests using multiple browsers and scenarios. It is often used as a QA for the front end, but also to execute E2E / integration testing from a user functionality perspective.

The job in this template assumes you will create a config at `$CI_PROJECT_DIR/cypress.json` (configurable) and that your tests can be found in the cypress directory at `$CI_PROJECT_DIR/cypress` (also configurable) and will execute them in a headless fashion. It will save artifacts such as the videos, screenshots and reports.

## Usage

### Available Variables

| Name | Default Value | Notes |
| ---  | ---           | --- |
| CYP_BASE_URL | `"http://$CI_PROJECT_NAME.$KUBE_NAMESPACE.svc.cluster.local"` | MANDATORY! Provide the base URL for the environment you are testing |
| CYP_CONFIG_FILE | `$CI_PROJECT_DIR/cypress.json` | Path to your cypress config (`--config`) |
| CYP_PROJECT | `$CI_PROJECT_DIR` | Path to where you base cypress tests directory can be found (`--project`) |
| CYP_REPORTER | `"junit"` | JUnit format is the format accepted for integrating with Gitlab UI (`--reporter`) |
| CYP_REPORTER_OPTS | `"mochaFile=cypress-results.xml,toConsole=true"` | (`--reporter-options`)|

### Default Usage 

The default case is simply to use the template as it is - providing only the environment's base URL for tests (will be exported as `$CYP_BASE_URL`)

```yaml
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/testing/cypress/integration-tests.yml'
variables:
  CYP_BASE_URL: "https://test.branch.domain.com"
```

### Custom Usage

Some basic things can be configured from the input variables but most configuration should happen from your cypress.json config file.

#### Example: Different location for the config and tests

```yaml
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/testing/cypress/integration-tests.yml'
integration-tests:
  variables:
    CYP_BASE_URL: "https://test.branch.domain.com"
    CYP_CONFIG_FILE: "another/path/cypress.json"
    CYP_PROJECT: "another/path/"
```

#### Example: Change when the job runs

Perhaps the tests should only run for master branch, in which case use normal Gitlab CI syntax to override the template

```yaml
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/testing/cypress/integration-tests.yml'
integration-tests:
  variables:
    CYP_BASE_URL: "https://test.branch.domain.com"

# Override template
integration-tests:
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_COMMIT_BRANCH == "master"
      when: on_success
      allow_failure: false
```
