FROM alpine:latest
RUN apk update --no-cache && \
    apk add "musl>=1.1.24-r10" "musl-utils>=1.1.24-r10" "libssl1.1>=1.1.1i-r0" "libcrypto1.1>=1.1.1i-r0"

CMD ["echo", "Hi"]
