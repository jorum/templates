# UNIT TEST (Golang)

[[_TOC_]]

## Description

This unit test job will:

- run your applications tests
- get code coverage results
- integrate both the results and the coverage into the Gitlab UI for statistics, Merge Requests and other areas too.

## Usage

### Available Variables

| Name | Default Value | Notes |
| ---  | ---           | --- |
| UT_WORK_DIR | `$CI_PROJECT_DIR` | The location from which your unit test scripts should run |

### Default Usage 

The default case (if you have configured your tests to work like the default variables) is simply to use the template as it is.

```yaml
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/unit-test/golang/unit-tests.yml'
```

### Custom Usage

#### If your tests should not run from the root of your project directory

```yaml
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/unit-test/golang/unit-tests.yml'
unit-tests:
  variables:
    UT_WORK_DIR: "/app/tests"
```
