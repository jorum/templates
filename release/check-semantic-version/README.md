# CHECK SEMANTIC VERSION FORMAT

[[_TOC_]]

## Description

A simple job that verifies that when tagging a release, it is in the correct format.

The default is checking for a tag in the format:

```
v0.0.1
v123.456.789
v111.12.0
etc
```

If you wish to change the format - then an example is provided below

## Usage of this custom template

### Available Variables

| Name | Default Value | Notes |
| ---  | ---           | --- |
| SEM_VER_REGEX | `"^v[0-9]+\.[0-9]+\.[0-9]+$"` | Checks for format `vX.Y.Z` |

### Default Usage 

The default case is simply to use the template as it is.

```yaml
include:
    - remote: 'https://gitlab.com/ingka/templates/raw/master/release/check-semantic-version/check-semver.yml'
```

### Custom Usage

#### To use a different acceptable format such as v-X-Y-Z

```yaml
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/release/check-semantic-version/check-semver.yml'
check-semver:
  variables:
    SEM_VER_REGEX: "^v\-[0-9]+\-[0-9]+\-[0-9]+$"
```
