# Find Sec Bugs (Java)

[[_TOC_]]

## Experimental!!!

Just a note to say that at this stage this job is very experimental as a template. SpotBugs and the FSB CLI are notoriously messy to configure and use. Many assumptions need to be made in order to make this a generic template and you may want to simply copy/paste the entire job to your .gitlab-ci.yml to tweak.

### Assumptions

- A previous job has already built the .jar you wish to scan (this is for speed - later I will provide an alternative version that builds the application too - but it will take twice as long to run)
- That the .jar exists in image used for this job (`image: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA`). An example of overriding the image is provided below.
- The same image has a functional JRE
- The same image has dos2unix available (the project keeps publishing the script with Windows line endings!)

### The Ugly

- The report can only be one format (limitation of fsb-cli.sh) and we prefer the HTML report as it is human readable. Thus the pass/fail is some very ugly HTML grepping reporting based on only on HIGH risk vulnerabilities.

## Description

Findsecbugs https://github.com/find-sec-bugs/find-sec-bugs/wiki/CLI-Tutorial is a tool for finding unsafe code in Java applications. If your project is a pure Java project and speed of pipeline is paramount, then this job can be useful instead of the Gitlab SAST job. It will be faster and skip a lot steps not required otherwise.

## Usage

### Available Variables

| Name | Default Value | Notes |
| ---  | ---           | --- |
| FSB_JAR | `"/app/myapp.jar"` | MANDATORY! Should provided full path to the .jar to scan |
| FSB_VERSION | `"1.11.0"` | The version of FindSecBugs CLI to use |
| FSB_ONLY_ANALYZE | `"com.ikea.-"` | Can specify multiple, comma seperated patterns |
| FSB_INCLUDE_XML | `$CI_PROJECT_DIR/fsb-include.xml` | |
| FSB_EXCLUDE_XML | `$CI_PROJECT_DIR/fsb-exclude.xml` | |
| FSB_EFFORT | `"max"` | |

### Default Usage 

The default case is simply to use the template as it is - just specify the path to the already built jar file that you wish to scan.

```yaml
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/sast/java/findsecbugs/sast-fsb.yml'
sast-fsb:
  variables:
    FSB_JAR: "/path/to/your/app.jar"
```

### Custom Usage

You can modify most things about the job using the variables above. More can be added if required. You can also use Gitlab syntax to overide when and how it runs too.

#### Example: Use another image that contains the jar

You can utilise normal Gitlab templating to override the `image: `

```yaml
include:
  - remote: 'https://gitlab.com/ingka/templates/raw/master/sast/java/findsecbugs/sast-fsb.yml'
# Override the template
sast-fsb:
  image: some-other-image:latest
  variables:
    FSB_JAR: "/path/to/your/app.jar"
```
